# thetravelers

Stuff related to the travelers web bases MMO puzzle game.
https://thetravelers.online/

calculator/index.html
Web page to calculate coordinates based on the formula that is in the step7 puzzle of the game.

alien/index.html
Web page to more easily convert the alien symbols to something we understand.
Bugs:
The display size is limited, if you have a complex equation it won't scroll once the box is full. However, the input is still there and is should be possibly to copy/paste.

Most of the code for this tool comes from: https://www.freecodecamp.org/news/how-to-build-an-html-calculator-app-from-scratch-using-javascript-4454b8714b98/
